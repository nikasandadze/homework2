package com.example.hwork2

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var editTextEmail : EditText
    private lateinit var editTextTextPassword : EditText
    private lateinit var editTextTextPassword2 : EditText
    private lateinit var buttonRegister : Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        registerlistener()

    }
    private fun init() {
        editTextEmail = findViewById(R.id.editTextEmail)
        editTextTextPassword = findViewById(R.id.editTextTextPassword)
        editTextTextPassword2 = findViewById(R.id.editTextTextPassword2)
        buttonRegister = findViewById(R.id.buttonRegister)


    }

    private fun registerlistener() {

        buttonRegister.setOnClickListener{
            val email = editTextEmail.text.toString()
            val password = editTextTextPassword.text.toString()
            val password2 = editTextTextPassword2.text.toString()

            if (password==password2 && !email.isEmpty() && password.length>=8) {
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(this, "Succes", Toast.LENGTH_SHORT).show()

                        } else {
                            Toast.makeText(this, "Check Email or Password", Toast.LENGTH_SHORT).show()
                        }

                    }
                Toast.makeText(this, "Succes", Toast.LENGTH_SHORT).show()
            }else {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }
            if (email.isEmpty() || password.isEmpty() || password2.isEmpty()) {
                Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


        }


    }


}